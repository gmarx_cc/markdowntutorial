<h1 id="formats-for-academic-documents-with-pandoc">Formats for academic documents with pandoc</h1>
<p>Markdown was originally developed by John Gruber in collaboration with Aaron Swartz, with the goal to simplify the writing of HTML <a href="http://daringfireball.net/projects/markdown/">documents</a>. Instead of coding a file in HTML syntax, the content of a document is written in <strong>plain text</strong> and annotated with <em>simple tags which define the formatting</em>. Subsequently, the Markdown (MD) files are parsed to generate the final HTML document. With this concept, the source file remains easily readable and the <strong>author can focus on the contents rather than formatting</strong>. Despite its original focus on the web, the MD format has been proven to be well suited for academic writing (Ovadia 2014). In particular, <a href="http://pandoc.org/">pandoc-flavored MD</a> adds several extensions which facilitate the authoring of academic documents and their conversion into multiple output formats. Tab. 2 demonstrates the simplicity of MD compared to other markup languages. Fig. 3 illustrates the generation of various formatted documents from a manuscript in pandoc MD. Some relevant functions for scientific texts are explained below in more detail.</p>
<figure>
<img src="Fig3.png" alt="pandoc flow process" /><figcaption>pandoc flow process</figcaption>
</figure>
<h1 id="pandoc-markdown-for-scientific-documents">Pandoc markdown for scientific documents</h1>
<p>In the following section, we demonstrate the potential for typesetting scientific manuscripts with pandoc using examples for typical document elements, such as tables, figures, formulas, code listings and references. A brief introduction is given by Dominici (2014). The complete Pandoc User’s Manual is available at <a href="http://pandoc.org/MANUAL.html">Manual</a>.</p>
<h2 id="tables">Tables</h2>
<p>There are several options to write tables in markdown. The most flexible alternative - which was also used for this article - are pipe tables. The contents of different cells are separated by pipe symbols (|):</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode markdown"><code class="sourceCode markdown"><a class="sourceLine" id="cb1-1" title="1">|Left | Center | Right | Default|</a>
<a class="sourceLine" id="cb1-2" title="2">|:----- |:------:|------:|---------|</a>
<a class="sourceLine" id="cb1-3" title="3">| LLL  | CCC    | RRR   | DDD|</a></code></pre></div>
<table>
<thead>
<tr class="header">
<th style="text-align: left;">Left</th>
<th style="text-align: center;">Center</th>
<th style="text-align: right;">Right</th>
<th>Default</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">LLL</td>
<td style="text-align: center;">CCC</td>
<td style="text-align: right;">RRR</td>
<td>DDD</td>
</tr>
</tbody>
</table>
<p>The headings and the alignment of the cells are given in the first two lines. The cell width is variable. The pandoc parameter <code>--columns=NUM</code> can be used to define the length of lines in characters. If contents do not fit, they will be wrapped.</p>
<p>Complex tables, e.g. tables featuring multiple headers or those containing cells spanning multiple rows or columns, are currently not representable in markdown format. However, it is possible to embed LaTeX and HTML tables into the document. These format-specific tables will only be included in the output if a document of the respective format is produced. This is method can be extended to apply any kind of format-specific typographic functionality which would otherwise be unavailable in markdown syntax.</p>
<h2 id="images-and-figures">Images and figures</h2>
<p>Images are inserted as follows:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode markdown"><code class="sourceCode markdown"><a class="sourceLine" id="cb2-1" title="1"><span class="al">![alt text](markdownLogo.png)</span>{width=30%}</a></code></pre></div>
<p>and produce this:</p>
<figure>
<img src="markdownLogo.png" alt="alt text" style="width:30.0%" /><figcaption>alt text</figcaption>
</figure>
<div class="sourceCode" id="cb3"><pre class="sourceCode markdown"><code class="sourceCode markdown"><a class="sourceLine" id="cb3-1" title="1"><span class="al">![alt text](markdownLogo.png)</span>{width=50%}</a></code></pre></div>
<p>and produce this: <img src="markdownLogo.png" alt="alt text" style="width:50.0%" /></p>
<p>The alt text is used e.g. in HTML output. Image dimensions can be defined in braces, as well, an identifier for the figure can be defined with #, resulting e.g. in the image attributes;<strong>only valid in pandoc</strong>:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode markdown"><code class="sourceCode markdown"><a class="sourceLine" id="cb4-1" title="1">{#figure1 height=30%}.</a></code></pre></div>
<p>A paragraph containing only an image is interpreted as a figure. The alt text is then output as the figure’s caption.</p>
<h2 id="symbols">Symbols</h2>
<p>Scientific texts often require special characters, e.g. Greek letters, mathematical and physical symbols etc.</p>
<p>The UTF-8 standard, developed and maintained by Unicode Consortium, enables the use of characters across languages and computer platforms.</p>
<p>In pandoc markdown documents, unicode characters such as °, α , ä , Å can be inserted directly and passed to the different output documents. The correct processing of MD with UTF-8 encoding to LaTeX/PDF output requires the use of the –latex-engine=xelatex option and the use of an appropriate font.</p>
<h2 id="formulas">Formulas</h2>
<p>Formulas are written in LaTeX mode using the delimiters <code>$</code>. E.g. the formula for calculating the standard deviation <span class="math inline">\(s\)</span> of a random sampling would be written as:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode markdown"><code class="sourceCode markdown"><a class="sourceLine" id="cb5-1" title="1">$s=\sqrt{\frac{1}{N-1}\sum_{i=1}^N(x_i-\overline{x})^{2}}$</a></code></pre></div>
<p>and produce this:</p>
<p><span class="math inline">\(s=\sqrt{\frac{1}{N-1}\sum_{i=1}^N(x_i-\overline{x})^{2}}\)</span></p>
<p>[s=]</p>
<h3 id="code-listings">Code Listings</h3>
<p>Verbatim code blocks are indicated by three tilde symbols:</p>
<pre><code>verbatim code</code></pre>
<p>Typesetting <code>inline code</code> is possible by enclosing text between back ticks.</p>
<h2 id="inserting-citations">Inserting citations</h2>
