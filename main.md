---
title: "The document title"
date: "October 2019"
author: 
- Author One [affiliation: University of Somewhere], and 
- Author Two [affiliation: University of Nowhere]
bibliography: references.bib 
csl: ieee.csl
---

# Formats for academic documents with pandoc
Markdown was originally developed by John Gruber in collaboration with Aaron Swartz, with the goal to simplify the writing of HTML [documents](http://daringfireball.net/projects/markdown/). Instead of coding a file in HTML syntax, the content of a document is written in **plain text** and annotated with *simple tags which define the formatting*. Subsequently, the Markdown (MD) files are parsed to generate the final HTML document. With this concept, the source file remains easily readable and the **author can focus on the contents rather than formatting**. Despite its original focus on the web, the MD format has been proven to be well suited for academic writing (Ovadia 2014). In particular, [pandoc-flavored MD](http://pandoc.org/) adds several extensions which facilitate the authoring of academic documents and their conversion into multiple output formats. Tab. 2 demonstrates the simplicity of MD compared to other markup languages. Fig. 3 illustrates the generation of various formatted documents from a manuscript in pandoc MD. Some relevant functions for scientific texts are explained below in more detail.

![pandoc flow process](Fig3.png){width=30%}

# Pandoc markdown for scientific documents

In the following section, we demonstrate the potential for typesetting scientific manuscripts with pandoc using examples for typical document elements, such as tables, figures, formulas, code listings and references. A brief introduction is given by Dominici (2014). The complete Pandoc User’s Manual is available at [Manual](http://pandoc.org/MANUAL.html).

## Tables 
There are several options to write tables in markdown. The most flexible alternative - which was also used for this article - are pipe tables. The contents of different cells are separated by pipe symbols (|):



``` markdown
|Left | Center | Right | Default|
|:----- |:------:|------:|---------|
| LLL  | CCC    | RRR   | DDD|
``` 

|Left | Center | Right | Default|
|:-----|:------:|------:|---------| 
| LLL  | CCC    | RRR   | DDD|

The headings and the alignment of the cells are given in the first two lines. The cell width is variable. The pandoc parameter `--columns=NUM` can be used to define the length of lines in characters. If contents do not fit, they will be wrapped.

Complex tables, e.g. tables featuring multiple headers or those containing cells spanning multiple rows or columns, are currently not representable in markdown format. However, it is possible to embed LaTeX and HTML tables into the document. These format-specific tables will only be included in the output if a document of the respective format is produced. This is method can be extended to apply any kind of format-specific typographic functionality which would otherwise be unavailable in markdown syntax.

## Images and figures 

Images are inserted as follows:

```markdown
![alt text](markdownLogo.png){width=30%}
```
and produce this: 

![alt text](markdownLogo.png){width=30%}

```markdown
![alt text](markdownLogo.png){width=50%}
```

and produce this:
![alt text](markdownLogo.png){width=50%}

The alt text is used e.g. in HTML output. Image dimensions can be defined in braces, as well, an identifier for the figure can be defined with #, resulting e.g. in the image attributes;**only valid in pandoc**:

```markdown
{#figure1 height=30%}.
```


A paragraph containing only an image is interpreted as a figure. The alt text is then output as the figure’s caption.

## Symbols

Scientific texts often require special characters, e.g. Greek letters, mathematical and physical symbols etc.

The UTF-8 standard, developed and maintained by Unicode Consortium, enables the use of characters across languages and computer platforms. 

In pandoc markdown documents, unicode characters such as º, $\alpha$ , ä , $\hat{A}$ can be inserted directly and passed to the different output documents. The correct processing of MD with UTF-8 encoding to LaTeX/PDF output requires the use of the --latex-engine=xelatex option and the use of an appropriate font. 



## Formulas

Formulas are written in LaTeX mode using the delimiters `$`. E.g. the formula for calculating the standard deviation $s$ of a random sampling would be written as:

```markdown
$s=\sqrt{\frac{1}{N-1}\sum_{i=1}^N(x_i-\overline{x})^{2}}$
```
and produce this:

$s=\sqrt{\frac{1}{N-1}\sum_{i=1}^N(x_i-\overline{x})^{2}}$

### Code Listings

Verbatim code blocks are indicated by three tilde symbols:

~~~
verbatim code
~~~
Typesetting `inline code` is possible by enclosing text between back ticks.

## Inserting citations

Some text to cite [@Romero-Serrano].
